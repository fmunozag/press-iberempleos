<?php

class VCE_Adsense_Widget extends WP_Widget { 

	var $defaults;

	function __construct() {
		$widget_ops = array( 'classname' => 'vce_adsense_widget', 'description' => esc_html__('You can place Google AdSense or any JavaScript related code inside this widget', 'voice-buddy') );
		$control_ops = array( 'id_base' => 'vce_adsense_widget' );
		parent::__construct( 'vce_adsense_widget', esc_html__('Voice Adsense', 'voice-buddy'), $widget_ops, $control_ops );

		$this->defaults = array( 
				'title' => esc_html__('Adsense', 'voice-buddy'),
				'adsense_code' => '',
				'expand' => 0
			);
	}

	
	function widget( $args, $instance ) {
		extract( $args );
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		if($instance['expand']){
			$before_widget = preg_replace('/class="(.*)"/', 'class="$1 vce_adsense_expand"', $before_widget);
		}


		echo wp_kses_post( $before_widget );

		if ( !empty($title) ) {
			echo wp_kses_post( $before_title . $title . $after_title );
		}
		
		$adsense_code = !empty( $instance['adsense_code'] ) ? $instance['adsense_code'] : '';

		?>
		<div class="vce_adsense_wrapper">
			<?php echo do_shortcode( $adsense_code ); ?>
		</div>
	
		<?php
			echo wp_kses_post( $after_widget );
	}

	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['adsense_code'] = current_user_can('unfiltered_html') ? $new_instance['adsense_code'] : stripslashes( wp_filter_post_kses( addslashes($new_instance['adsense_code']) ) );
		$instance['expand'] = isset($new_instance['expand']) ? 1 : 0;
		return $instance;
	}

	function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, $this->defaults ); ?>
			
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Title', 'voice-buddy'); ?>:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr($instance['title']); ?>" class="widefat" />
			<small class="howto"><?php esc_html_e('Leave empty for no title', 'voice-buddy'); ?></small>
		</p>
		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'adsense_code' ) ); ?>"><?php esc_html_e('Adsense Code', 'voice-buddy'); ?>:</label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'adsense_code' ) ); ?>" type="text" name="<?php echo esc_attr( $this->get_field_name( 'adsense_code' ) ); ?>" class="widefat" rows="10"><?php echo esc_textarea( $instance['adsense_code'] ); ?></textarea>
			<small class="howto"><?php esc_html_e('Place your Google Adsense code or any JavaScript related advertising code.', 'voice-buddy'); ?></small>
		</p>

		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'expand' ) ); ?>" type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'expand' ) ); ?>" value="1" <?php checked(1, $instance['expand']); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'expand' ) ); ?>"><?php esc_html_e('Expand widget area to 300px', 'voice-buddy'); ?></label>
			<small class="howto"><?php esc_html_e('Check this option if you are using 300px wide adsense so it can fit your widget area', 'voice-buddy'); ?></small>
		</p>
				
	<?php
	}
}

?>