<?php

/**
 * Load admin js files
 *
 * @since  1.0
 */

add_action( 'admin_enqueue_scripts', 'voice_buddy_load_admin_js' );

function voice_buddy_load_admin_js() {

	global $pagenow, $typenow;	

	//Load widgets JS
	if ( $pagenow == 'widgets.php' ) {
		wp_enqueue_script( 'voice-widgets', VOICE_BUDDY_URL . 'assets/js/admin/widgets.js' , array( 'jquery' ), VOICE_BUDDY_VER );
	}


}

?>