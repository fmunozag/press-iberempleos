<?php

add_action( 'admin_init', 'voice_buddy_compatibility' );

function voice_buddy_compatibility() {

	if ( is_admin() && current_user_can( 'activate_plugins' ) && !voice_buddy_is_theme_active() ) {

		add_action( 'admin_notices', 'voice_buddy_compatibility_notice' );

		deactivate_plugins( VOICE_BUDDY_BASENAME );

		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}
}

function voice_buddy_compatibility_notice() {
	echo '<div class="notice notice-warning"><p><strong>Note:</strong> Voice Buddy plugin has been deactivated as it requires Voice Theme to be active.</p></div>';
}

function voice_buddy_is_theme_active() {
	return defined( 'VOICE_THEME_VERSION' );
}

?>
