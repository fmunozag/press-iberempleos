��          �            x     y     �  	   �     �     �     �  +   �       r   +     �     �     �     �     �     �  ,  �  (   ,     U     q     ~     �     �  6   �     �  �   �     ~     �     �     �     �     �                             	                    
                              Display your Flickr photostream Example ID: 23100287@N07 Flickr ID Meks Meks Flickr Widget Meks Simple Flickr Widget Note: You can use "0" value for auto height Number of photos Quickly display your Flickr photos inside WordPress widget. No authorization required (only provide your user id). Randomize photos? Thumbnail height Thumbnail width Title What's my Flickr ID? https://mekshq.com PO-Revision-Date: 2018-08-26 15:01:04+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Meks Simple Flickr Widget - Development (trunk)
 Muestra tu colección de fotos de Flickr ID de ejemplo: 23100287@N07 ID de Flickr Meks Meks Flickr Widget Meks Simple Flickr Widget Nota: puedes usar el valor "0" para altura automática Número de fotos Muestra fácilmente tus fotos de Flickr en un widget de WordPress. No se requiere autorización (solo es necesario tu ID de usuario). ¿orden aleatorio? Altura de la miniatura Anchura de la miniatura Título ¿Cuál es mi ID de Flickr? https://mekshq.com 