��          �   %   �      0     1     :  *   A  �   l  	   <     F     K  
   Z  	   e     o     u     �     �     �  
   �     �     �     �     �     �          	       +  .     Z     h  4   q  �   �     �     �     �     �     �     �  !   �     �     �          (     6     G     \     j     s     {          �                                                                                                           
         	           Add Icon Circle Display your social icons with this widget Easily display more than 100 social icons inside WordPress widget. Choose from different icon shapes and sizes and quickly connect your website with your social profiles. Aim, Apple, Behance, Blogger, Cargo, Follow Me Icon Icon font size Icon shape Icon size Icons Introduction text (optional) Meks Meks Smart Social Widget Meks Social Widget New Window Open links in Rounded corners Same Window Square Title Url http://mekshq.com https://mekshq.com PO-Revision-Date: 2018-08-26 15:02:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Meks Smart Social Widget - Development (trunk)
 Añadir icono Círculo Muestra iconos de tus redes sociales con este widget Muestra fácilmente más de 100 iconos de redes sociales en un widget de WordPress. Elige entre diferentes formas y tamaños y conecta rápidamente tu web con tus perfiles sociales. Aim, Apple, Behance, Blogger, Cargo, Sígueme Icono Tamaño de fuente del icono Forma del icono Tamaño del icono Iconos Texto de introducción (opcional) Meks Meks Smart Social Widget Meks Social Widget Nueva ventana Abrir enlaces en Esquinas redondeadas Misma ventana Cuadrado Título URL http://mekshq.com https://mekshq.com 