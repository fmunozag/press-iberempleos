<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

<head>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11" />
<?php wp_head(); ?>

	<!-- Google Tag Manager -->        <script>        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);        })(window,document,'script','dataLayer','GTM-T5T5RCP');        </script>        <!-- End Google Tag Manager -->
	
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5T5RCP"        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>        <!-- End Google Tag Manager (noscript) -->


<div class="media_header" style="background: #fff;">
	<?php 
		
		if( isset($_COOKIE["session_cab"]) )
		{
			$html_cabecera = file_get_contents('https://www.iberempleos.es/cabeceras/cabecera_' . $_COOKIE["session_cab"] . '.html');
	
			echo $html_cabecera;
		}
	?>
</div>



<div id="vce-main">

<header id="header" class="main-header">
<?php if(vce_get_option('top_bar')) : ?>
	<?php get_template_part('template-parts/headers/top'); ?>
<?php endif; ?>
<?php get_template_part('template-parts/headers/header-'.vce_get_option('header_layout')); ?>
</header>

<?php if( vce_get_option( 'sticky_header' ) ): ?>
	<?php get_template_part('template-parts/headers/sticky'); ?>
<?php endif; ?>

<div id="main-wrapper">
