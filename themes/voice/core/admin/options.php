<?php

/* Load the embedded Redux Framework */

if ( ! class_exists( 'Redux' ) ) {
    return;
}

/**
 * Redux params
 */

$opt_name = 'vce_settings';

$args = array(
    'opt_name'             => $opt_name,
    'display_name'         => wp_kses_post( sprintf( __( 'Voice Options%sTheme Documentation%s', 'voice' ), '<a href="https://mekshq.com/documentation/voice" target="_blank">', '</a>' ) ),
    'display_version'      => vce_get_update_notification(),
    'menu_type'            => 'menu',
    'allow_sub_menu'       => true,
    'menu_title'           => esc_html__( 'Theme Options', 'voice' ),
    'page_title'           => esc_html__( 'Voice Options', 'voice' ),
    'google_api_key'       => '',
    'google_update_weekly' => false,
    'async_typography'     => true,
    'admin_bar'            => true,
    'admin_bar_icon'       => 'dashicons-admin-generic',
    'admin_bar_priority'   => '100',
    'global_variable'      => '',
    'dev_mode'             => false,
    'update_notice'        => false,
    'customizer'           => false,
    'allow_tracking' => false,
    'ajax_save' => false,
    'page_priority'        => '27.11',
    'page_parent'          => 'themes.php',
    'page_permissions'     => 'manage_options',
    'menu_icon'            => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCAzMDAgMzAwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMDAgMzAwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+PHBhdGggZD0iTTIwOS40LDYxLjZsLTE5LjctM1YzOS41aDY5djE5LjFMMjQxLjIsNjFsLTczLjQsMTk3LjVoLTMxLjIiLz48L2c+PGc+PHBhdGggZD0iTTE0NS44LDIwMWwtMTQuOSw0MC4yTDU4LDYxbC0xNy42LTIuNFYzOS41aDY5djE5LjFsLTE5LjcsMyIvPjwvZz48L3N2Zz4=',
    'last_tab'             => '',
    'page_icon'            => 'icon-themes',
    'page_slug'            => 'vce_options',
    'save_defaults'        => true,
    'default_show'         => false,
    'default_mark'         => '',
    'show_import_export'   => true,
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => false,
    'output_tag'           => true,
    'database'             => '',
    'system_info'          => false,
);

$GLOBALS['redux_notice_check'] = 1;



$args['intro_text'] = '';
$args['footer_text'] = '';

Redux::setArgs( $opt_name, $args );


include_once get_parent_theme_file_path( 'core/admin/options-fields.php' );


/* Append custom css to redux framework */
if ( !function_exists( 'vce_redux_custom_css' ) ):
    function vce_redux_custom_css() {
        wp_register_style( 'vce-redux-custom-css', get_parent_theme_file_uri( 'assets/css/admin/options.css' ), array( 'redux-admin-css' ), VOICE_THEME_VERSION );
        wp_enqueue_style( 'vce-redux-custom-css' );
    }
endif;

add_action( 'redux/page/vce_settings/enqueue', 'vce_redux_custom_css' );


/* Remove redux framework admin page to avoid confusion of our users and unnecesarry support questions */
if ( !function_exists( 'vce_remove_redux_page' ) ):
    function vce_remove_redux_page() {
        remove_submenu_page( 'tools.php', 'redux-about' );
    }
endif;

add_action( 'admin_menu', 'vce_remove_redux_page', 99 );


/* Prevent redux auto redirect */
update_option( 'redux_version_upgraded_from', 100 );


/* More redux cleanup, blah... */

add_action( 'init', 'vce_redux_cleanup' );

if ( !function_exists( 'vce_redux_cleanup' ) ):
    function vce_redux_cleanup() {

        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
        }
    }
endif;


/**
 * Add our section custom field to redux
 *
 * @since  1.0
 */

if ( !function_exists( 'vce_section_field_path' ) ):
    function vce_section_field_path( $field ) {
        return get_parent_theme_file_path( 'core/admin/options-custom-fields/section/section.php' );
    }
endif;

add_filter( "redux/vce_settings/field/class/vce_section", "vce_section_field_path" );

?>